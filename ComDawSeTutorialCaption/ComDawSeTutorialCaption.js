var CAPTION_ALIGNMENT_MIDDLE = "middle";
var CAPTION_ALIGNMENT_LEFT = "left";
var CAPTION_ALIGNMENT_RIGHT = "right";
var ComDawSeTutorialCaption = function(box, optionclass) {
    ComDawSeTutorialCaption.baseConstructor.call(this, box, optionclass);
};

//Classroom Heritage
mynfc.extending(ComDawSeTutorialCaption, Thing );

//init
ComDawSeTutorialCaption.prototype.init = function(){
    this.child = this.spec.screen['icon'];
    this.textPx;
};

//onPaint()
ComDawSeTutorialCaption.prototype.onPaint = function(){
    //Simple display
    Thing.prototype.onPaint.call(this);
    var scene = getCurrentScene();
    scene.setFont(this.spec.innerfont);
    scene.setTextColor(this.spec.color);
    this.textPx = this.spec.text;
    var x;
    var y;
    var withMargin;

    withMargin = this.getWidth()-(this.child.getWidth()+this.child.spec.x);
    //Cut Part
    this.textPx = this.cut(this.spec.text,this.spec.innerfont, withMargin);
    // Alignment
    if(this.spec.alignment == CAPTION_ALIGNMENT_LEFT){
        x = this.getX() + this.child.getWidth() + this.child.spec.x;
    }else if(this.spec.alignment == CAPTION_ALIGNMENT_RIGHT){
        x = this.getX() + this.getWidth() - this.child.getWidth() - scene.getTextWidth(this.textPx)-this.child.spec.x;
    }else if(this.spec.alignment == CAPTION_ALIGNMENT_MIDDLE){
        withMargin = this.getWidth()-(this.child.getWidth()*2+this.child.spec.x);
        this.textPx = this.cut(this.spec.text,this.spec.innerfont, withMargin);
        x = this.getX() + this.getWidth()/2 - scene.getTextWidth(this.textPx)/2;
    }
    y = this.getY() + this.getHeight()/2 - scene.getTextHeight(this.spec.text)/2;



    //Display
    scene.drawString(this.textPx, x , y);
};

//Cut function
ComDawSeTutorialCaption.prototype.cut = function(text, font, width){
    if (getCurrentScene().getTextWidth(text) > width) {
        getCurrentScene().setFont(font); // if the font changed, the function will still work
        var res;
        var w = getCurrentScene().getTextWidth(text);
        var i = text.length;
        while (w >= width) {
            i--;
            res = text.substring(0, i);
            w = getCurrentScene().getTextWidth(res);
        }
        res = text.substring(0, i - 1);
        return res;
    }else{
        return text;
    }
};

/*
* This is the second Thing, for drawing the icon
*/

var ComDawSeTutorialIcon = function(box, optionclass){
    ComDawSeTutorialIcon.baseConstructor.call(this, box, optionclass);
};

mynfc.extending(ComDawSeTutorialIcon, Thing);

ComDawSeTutorialIcon.prototype.init = function(){
    this.parent = this.spec.screen['Caption'];
};

ComDawSeTutorialIcon.prototype.onPaint = function(){
    var scene2 = getCurrentScene();
    var x2;
    var x3;
    var y2;
    if(this.parent.spec.alignment == CAPTION_ALIGNMENT_LEFT){
        x2 = this.getX();
    }else if(this.parent.spec.alignment == CAPTION_ALIGNMENT_RIGHT){
        x2 = this.parent.getX() + this.parent.getWidth() - this.getWidth() - this.spec.x;
    }else if(this.parent.spec.alignment == CAPTION_ALIGNMENT_MIDDLE){
        x2 = this.parent.getX() + this.parent.getWidth()/2 - scene2.getTextWidth(this.parent.textPx)/2 - this.getWidth();
        x3 = this.parent.getX() + this.parent.getWidth()/2 + scene2.getTextWidth(this.parent.textPx)/2;
    }

    y2 = this.getY() + this.parent.getHeight()/2 - this.getHeight()/2;
    scene2.drawImage(this.spec.url,x2,y2);
    if(this.parent.spec.alignment == CAPTION_ALIGNMENT_MIDDLE) {
        scene2.drawImage(this.spec.url, x3, y2);
    }
};